package com.activity.example;
import java.util.Scanner;

public class activity {
   public static void main(String[] args){

       Scanner scanner = new Scanner(System.in);

       String firstName, lastName;
       double firstSubject, secondSubject, thirdSubject;

        System.out.println("First Name:");
        firstName = scanner.next();
        System.out.println("Last Name:");
        lastName = scanner.next();
        System.out.println("First Subject Grade:");
        firstSubject = scanner.nextDouble();
        System.out.println("Second Subject Grade:");
        secondSubject = scanner.nextDouble();
        System.out.println("Third Subject Grade:");
        thirdSubject = scanner.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName);
        int avg = (int) ((firstSubject + secondSubject + thirdSubject) / 3);
        System.out.println("Your grade average is : " + (avg));
   }
}
